import React from 'react';
import './App.css';
import Navbar from './components/navbar'
import Products from './components/products'
import Slider from './components/Slider'

function App() {
  return (
    <React.Fragment>
      <Navbar/>
      <Products/>
    <Slider/>
    </React.Fragment>
  );
}

export default App;
