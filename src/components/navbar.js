import React from 'react';
import '../styles/navbar.css';

function Navbar() {
  return (
      <React.Fragment>

        <header class="containerNavbars">
          <div class="row col-md-12">
            <nav class="navbar navbar-expand-lg navbar-light bg-light navbarStyle">
              <div class="col-md-1">
                <a class="navbar-brand"  href="#" id="logo" ></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                  <span class="navbar-toggler-icon">ddd</span>
                </button>
              </div>
              <div class="collapse navbar-collapse col-md-11 " id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto col-md-12">

                  <li class="nav-item active col-md-9">

                    <div class="col-md-12 ">
                      <form class="form-inline">
                        <div class="dropdown col-md-3 divButton">
                          <button class="btn btn-secondary dropdown-toggle allCategoriesDropDown" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Tutte le categorie
                          </button>
                          <div class="dropdown-menu menuStyle" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" id="primeAmenu" href="#">Informatica</a>
                            <a class="dropdown-item" href="#">Sport</a>
                            <a class="dropdown-item" href="#">Cucina</a>
                          </div>

                        </div>
                        <div class="col-md-8 ">
                          <div class="col-md-10 backgroundSearchbar">
                            <input class="form-control mr-sm-2 col-md-11 floatElementsSearch" id="prova"  type="text" placeholder="Search" ></input>
                            <button  class="col-md-1 floatElementsSearch" type="submit" id="lente"></button>
                          </div>
                        </div>
                      </form>
                    </div>
                  </li>
                  <div class="col-md-3 buttonsNavbar">
                    <li class="nav-item col-md-5">
                      <button class="btn btn-primary colorButton" type="button">Accedi</button>
                    </li>
                    <li class="nav-item col-md-5">
                      <button class="btn btn-primary colorButton" type="button">Registrati</button>
                    </li>
                    <li className="nav-item col-md-2">
                      <button className="col-md-1 imgCarrello floatElementsSearch" type="button" ></button>
                    </li>
                  </div>
                </ul>
              </div>
            </nav>
          </div>



          <div class="col-md-12 marginSN removePadding">
            <nav class="navbar navbar-expand-lg navbar-light bg-light sottoNavbar col-md-12 removePadding">

              <div class="collapse navbar-collapse col-md-12 containerSottoNavbar" id="navbarNav">
                <ul class="navbar-nav col-md-12 removePadding">
                  <div className="col-md-9 floatElementsSearch vedi removePadding">
                    <li class="nav-item active">
                      <button class="butSottoNavbar first" type="button">Categoria 1</button>
                    </li>
                    <li class="nav-item">
                      <button class="butSottoNavbar" type="button">Categoria 2</button>
                    </li>
                    <li class="nav-item">
                      <button class="butSottoNavbar" type="button">Categoria 3</button>
                    </li>
                    <li class="nav-item">
                      <button class="butSottoNavbar" type="button">Categoria 4</button>
                    </li>

                  </div>
                  <div class="col-md-3 bannerAds floatElementsSearch removePadding">


                  </div>
                </ul>
              </div>
            </nav>

          </div>


        </header>
      </React.Fragment>
  );
}

export default Navbar;