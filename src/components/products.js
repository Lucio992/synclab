import React from 'react';
import '../styles/products.css';

function Products() {
    return (
        <React.Fragment>
            <div class="containerCards col s12 m5">
                <div class="rowCard1">
                    <div class="dimensionCards" id="a">
                        <div class="col s12 m3 dimensionCard">
                            <div class="card dimensionCard  module-border-wrap">
                                <div class="card-image">
                                    <span class="card-title titleOfCard1">Pallone basket</span>
                                    <div class="imgCard1"></div>
                                </div>
                                <div class="card-content descriptionCard1">
                                    Pallone arancione, diviso da piccole scanalature nere in otto spicchi, è fatto di cuoio, gomma o materiali sintetici.
                                    <br></br>
                                    La circonferenza varia da 74,9-78 cm .
                                </div>
                                <div class="card-action linkCard1">
                                    <a href="#">Visualizza dettagli</a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="dimensionCards" id="b">

                    </div>

                    <div class="dimensionCards" id="c">

                    </div>
                </div>

                <div  class="rowCard2">
                    <div class="dimensionCards" id="d">

                    </div>

                    <div class="dimensionCards" id="e">




                    </div>

                    <div class="dimensionCards" id="f">

                    </div>
                </div>


                <div class="rowCard3">
                    <div class="dimensionCards" id="g">

                    </div>

                    <div class="dimensionCards" id="h">

                    </div>

                    <div class="dimensionCards" id="i">

                    </div>
                </div>
            </div>

        </React.Fragment>
    );
}

export default Products;