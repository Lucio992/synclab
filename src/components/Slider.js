import React ,{useState} from "react";
import '../styles/Slider.scss'
import ImgComp from "./ImgComp";
import i1 from "./pics/imageIT (1).jpg"
function Slider() {
    let sliderArr = [<ImgComp src={i1}/>,2,3,4,5] ;
    const [x,setX] = useState(0);
    const goLeft = () => {
        x=== 0 ? setX(-100*(sliderArr.length-1)):setX(x+100);
    }
    const goRight = () => {
        x=== -100*(sliderArr.length-1)?setX(0):setX(x-100);
    } ;
    return(
        <div className="slider">
            {
                sliderArr.map((item,index) =>{
                    return(
                        <div key={index} className="slide" style={{transform:`translateX(${x}%)`}}>
                            {item}
                        </div>
                    )

                })}
            <button id="goLeft" onClick={goLeft}><i className="fas fa-angle-left"></i></button>
            <button id="goRight" onClick={goRight} ><i className="fas fa-angle-right"></i></button>
        </div>
    )

}

export default Slider;